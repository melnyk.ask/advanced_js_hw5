async function getUsers(){
    let request_users = await fetch("https://ajax.test-danit.com/api/json/users");
    let users = await request_users.json();
    let request_posts = await fetch("https://ajax.test-danit.com/api/json/posts");
    let posts = await request_posts.json();
    // console.log(users);
    // console.log(posts);
    return {
        "users": users,
        "posts": posts
    };
}


class Card {
    constructor(name, email, title, text, user_id, post_id){
        this.name = name;
        this.email = email;
        this.title = title;
        this.text = text;        
        this.user_id = user_id;
        this.post_id = post_id;
    }
}

async function users_posts_to_classes() {

    //add_form
    let add_post = document.querySelector(".add_post"); 
    let add_btn = document.querySelector(".add_btn");
    let post_title = document.querySelector(".post_title");
    let post_text = document.querySelector(".post_text");
    let yes = document.querySelector(".yes");
    let no = document.querySelector(".no");

    // //edit_form
    let edit_post = document.querySelector(".edit_post");

    let increase = 100;

    let out = document.querySelector(".out");
    out.innerHTML = "";

    let div_a = document.createElement("div");
    div_a.classList.add("div_a");
    out.append(div_a);

    add_btn.onclick = () => {
        post_title.value = "";
        post_text.value = "";
        add_post.classList.remove("hidden");
        edit_post.classList.add("hidden");
    }

    no.onclick = () => {
        add_post.classList.add("hidden");
    }

    yes.onclick = async () => {
        add_post.classList.add("hidden");
        // console.log(post_title.value, post_text.value);
        let div_to_insert = document.createElement("div");

        let name_to_insert = "";
        let email_to_insert = "";
        let user_id_to_insert = "";
        let post_id_to_insert = "";

        for(let i = 0; i < cards_array.length; i++){
            if(cards_array[i].user_id === 1){
                name_to_insert = cards_array[i].name;
                email_to_insert = cards_array[i].email;
                user_id_to_insert = cards_array[i].user_id;
                post_id_to_insert = cards_array[i].post_id + increase;
                increase++;
                break;
            }
        }

        div_to_insert.innerHTML = `
        
            <div class="name_mail">
                <h2 class="name">${name_to_insert}</h2>
                <p class="mail">${email_to_insert}</p>
            </div>        
            <h3 class="title_h3">${post_title.value}</h3>        
            <p class="text">${post_text.value}</p>
            <div class="buttons">
                <div class="del btn_style" onclick = del(${post_id_to_insert}) >DEL</div>
                <img src="./edit.png" alt="edit_icon" class="edit_icon" onclick = edit(${post_id_to_insert})>
            </div>
        
        `;

        div_to_insert.classList.add("card");
        div_to_insert.classList.add(user_id_to_insert);
        div_to_insert.classList.add(`post_id${post_id_to_insert}`);

        out.prepend(div_to_insert);

        let post_request = await fetch("https://ajax.test-danit.com/api/json/posts", {
            method: 'POST',
            body: JSON.stringify({
                body: post_text.value,
                id: post_id_to_insert,
                title: post_title.value,
                userId: user_id_to_insert
            }),
            headers: {
            'Content-Type': 'application/json'
            }
        });
}


    // await delay(5000);

    let all = await getUsers();
    // console.log(all);
    let cards_array = [];
    
    for(let i = 0; i<all.users.length; i++){
        for(let j = 0; j<all.posts.length; j++){
            if(all.users[i].id === all.posts[j].userId){

                let current_card = new Card(
                    all.users[i].name,
                    all.users[i].email,
                    all.posts[j].title,
                    all.posts[j].body,
                    all.users[i].id, //user ID
                    all.posts[j].id  //post ID
                );
                cards_array[j] = current_card;
            }
        }
    }

    // console.log("cards_array:",cards_array); 

    div_a.remove();

    for(let i = 0; i < cards_array.length; i++){
        out.innerHTML += `
        <div class="card ${cards_array[i].user_id} post_id${cards_array[i].post_id}">
            <div class="name_mail">
                <h2 class="name">${cards_array[i].name}</h2>
                <p class="mail">${cards_array[i].email}</p>
            </div>        
            <h3 class="title_h3">${cards_array[i].title}</h3>        
            <p class="text">${cards_array[i].text}</p>
            <div class="buttons">
                <div class="del btn_style" onclick = del(${cards_array[i].post_id}) >DEL</div>
                <img src="./edit.png" alt="edit_icon" class="edit_icon" onclick = edit(${cards_array[i].post_id})>
            </div>
        </div>
        `;
    }
    // return cards_array;      
}

users_posts_to_classes();

async function del(post_id) {
    let del_request = await fetch(`https://ajax.test-danit.com/api/json/posts/${post_id}`);

    // console.log(del_request.ok, del_request.status);

    if(del_request.ok===true && del_request.status===200){
        document.querySelector(`.post_id${post_id}`).remove();
    }    
}

//--------------------------------------------------------------------
//edit_post - form;
async function edit(post_id) {
    let add_post = document.querySelector(".add_post"); 

    let form_to_edit = document.querySelector(`.post_id${post_id}`);

    let title_to_edit = document.querySelector(`.post_id${post_id} .title_h3`);
    let text_to_edit = document.querySelector(`.post_id${post_id} .text`);
    //edit_form
    let edit_post = document.querySelector(".edit_post");
    let edit_post_title = document.querySelector(".edit_post_title");
    let edit_post_text = document.querySelector(".edit_post_text");
    let edit_yes = document.querySelector(".edit_yes");
    let edit_no = document.querySelector(".edit_no");

    add_post.classList.add("hidden");
    edit_post.classList.remove("hidden");

    edit_post_title.value = title_to_edit.textContent;
    edit_post_text.value = text_to_edit.textContent;

    edit_yes.onclick = async () => { 
        title_to_edit.textContent = edit_post_title.value;
        text_to_edit.textContent = edit_post_text.value;
        edit_post.classList.add("hidden");

        await fetch(`https://ajax.test-danit.com/api/json/posts/${post_id}`, {
        method: 'PUT',
        body: JSON.stringify({
                body: text_to_edit.textContent,
                id: post_id,
                title: title_to_edit.textContent,
                userId: +form_to_edit.classList[1] 
            }),
        headers: {
            'Content-Type': 'application/json'
        }
        });
        
    }

    edit_no.onclick = () => { 
        edit_post.classList.add("hidden");
    }
}
//--------------------------------------------------------------------

async function delay(ms){
    await new Promise(
        (res) => {
            setTimeout(
                res, ms
            )
        }
    )
}

